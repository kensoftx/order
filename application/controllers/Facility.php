<?php 
if( !defined('BASEPATH') ) exit('No direct script access allowed');

class Facility extends CI_Controller 
{


	public function __construct()
	{
  		parent::__construct();
		
		 if (!$this->session->userdata('is_logged_in')) {
            redirect('authentication');
		} 
		
		$this->load->model("facilities");
		$this->load->model("counties");
		$this->data['parent_page'] = 'Facility';
		$this->data['home'] = 'Home';
		$this->_ci =& get_instance();
	

  	}
  	public function index(){
	//	echo 1;exit;

		$this->form_validation->set_rules( [
			['field' => 'facility_name', 'label' => 'Facility', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
			// [
			// 	'field' => 'corporate_name', 
			// 	'label' => 'Corporate Name', 
			// 	'rules' => 'trim|required|is_unique[facilities.corporate_name]',
			// 	'errors' => ['required' => 'Please enter the %s.','is_unique' => 'This %s is already in use.',],
			// ],
		
										
		] );
		
		if( $this->form_validation->run() == FALSE ) :
			
			$this->data['title'] = "Ordering:: Facilities";
			$this->data['page'] = 'Facility/Add';
			$this->data['county']=$this->counties->counties();
			$this->data['sub_county']=$this->facilities->sub_counties($this->input->post("county_name"));
			$this->load->view( "templates/header", $this->data );
			$this->load->view( 'facility/add' );		
			$this->load->view( "templates/side_menu" );			
			$this->load->view( "templates/footer" );
		else :
			if($this->facilities->saves($this->_ci->input->user_agent(),$this->_ci->input->ip_address())){
			 
					$this->session->set_flashdata( "success", "Facility created successfully " );
					redirect( 'facility' );
				
			}else{

				$this->session->set_flashdata( "error", "An error occurred while creating Facility" );
				redirect( 'facility' );
			}
			
			
		endif;	
	}

	
	 function get_sub_county()
		{
		    $options = array();
		    $options[]='Select Ward'; 
		    for( $i=0; $i<count( $this->data['wards']  ); $i++ ) :
		      $warding = &$this->data['wards'] [$i];
		      $options[$warding->id] = $warding->name; 
		    endfor;   
		    echo json_encode( $options );
		}

	public function lists()
	{
			$this->data['title'] = "Ordering:: Facilities";
			$this->data['page'] = 'Facility/List';
			$this->data['records']=$this->facilities->lists();
			$this->load->view( "templates/header", $this->data );
			$this->load->view( 'facility/lists' );
			$this->load->view( "templates/side_menu" );
			$this->load->view( "templates/footer" );
		
	} 

	public function view()
	{
			$this->data['title'] = "Ordering:: Products";
			$this->data['page'] = 'Facility/View';
			$this->data['records']=$this->facilities->view();
			$this->load->view( "templates/header", $this->data );
			$this->load->view( 'facility/view' );
			$this->load->view( "templates/side_menu" );
			$this->load->view( "templates/footer" );
		
	} 
	
  

}