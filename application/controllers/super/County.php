<?php 
if( ! defined('BASEPATH') ) exit( 'No direct script access allowed' );

class County extends CI_Controller
{				
	protected $data = array();
	
	public function __construct() 
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
            redirect('authentication');
		}

		//$this->genlib->Su();
		$this->load->helper('form');
		$this->load->model(['counties']);
		$this->data['parent_page'] = 'County';
		$this->data['home'] = 'Home';
		$this->_ci =& get_instance();
		
	}
	
	public function index()
	{   

		$this->load->helper('form');
		$this->data['title'] = "Ordering:: County";
		$this->data['page'] = 'County/List';
		//$this->data['name']= $this->companies->backend();
		$this->data['records']= $this->counties->counties();
		$this->load->view( "templates/header", $this->data );
		$this->load->view( 'county/lists' );
		$this->load->view( "templates/side_menu" );			
		$this->load->view( "templates/footer" );	
	}

		public function add(){
	//	echo 1;exit;

		$this->form_validation->set_rules( [
			['field' => 'description', 'label' => 'Description', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
			[
				'field' => 'county_name', 
				'label' => 'County', 
				'rules' => 'trim|required|is_unique[counties.name]',
				'errors' => ['required' => 'Please enter the %s.','is_unique' => 'This %s is already in use.',],
			],
		
										
		] );
		
		if( $this->form_validation->run() == FALSE ) :
			
			$this->data['title'] = "Ordering:: County";
			$this->data['page'] = 'County/Add';
			$this->load->view( "templates/header", $this->data );
			$this->load->view( 'county/add' );		
			$this->load->view( "templates/side_menu" );			
			$this->load->view( "templates/footer" );
		else :
			if($this->counties->saves($this->_ci->input->user_agent(),$this->_ci->input->ip_address())){
			 
					$this->session->set_flashdata( "success", "County account successfully created" );
					redirect( 'super/county' );
				
			}else{

				$this->session->set_flashdata( "error", "An error occurred while creating County account" );
				redirect( 'super/county/add' );
			}
			
			
		endif;	
	}

	public function add_sub_county(){
	//	echo 1;exit;

		$this->form_validation->set_rules( [
			['field' => 'county', 'label' => 'County', 'rules' => 'trim|required', 'errors' => ['required' => 'Please select the %s.'],],
			[
				'field' => 'sub-county_name', 
				'label' => 'Sub-County', 
				'rules' => 'trim|required|is_unique[sub_counties.name]',
				'errors' => ['required' => 'Please enter the %s.','is_unique' => 'This %s is already in use.',],
			],
		
										
		] );
		
		if( $this->form_validation->run() == FALSE ) :
			
			$this->data['title'] = "Ordering:: Sub-County";
			$this->data['page'] = 'Sub-County/Add';
			$this->data['counties']=$this->counties->counties();
			$this->load->view( "templates/header", $this->data );
			$this->load->view( 'sub-county/add' );		
			$this->load->view( "templates/side_menu" );			
			$this->load->view( "templates/footer" );
		else :
			if($this->counties->saves_sub_county($this->_ci->input->user_agent(),$this->_ci->input->ip_address())){
			 
					$this->session->set_flashdata( "success", "Sub-County successfully created" );
					redirect( 'super/county/sub_county' );
				
			}else{

				$this->session->set_flashdata( "error", "An error occurred while creating Sub-County" );
				redirect( 'super/county/add_sub_county' );
			}
			
			
		endif;	
	}

	public function sub_county()
	{   

		$this->load->helper('form');
		$this->data['title'] = "Ordering:: Sub-County";
		$this->data['page'] = 'Sub-County/List';
		//$this->data['name']= $this->companies->backend();
		$this->data['records']= $this->counties->sub_counties();
		$this->load->view( "templates/header", $this->data );
		$this->load->view( 'sub-county/lists' );
		$this->load->view( "templates/side_menu" );			
		$this->load->view( "templates/footer" );	
	}


public function edit()
	{	
		$this->data['title'] = "Petropesa:: Edit Company";
		$this->data['page'] = 'Company/Edit';
		$this->data['fa']="fa fa-edit";
		$this->form_validation->set_rules( [
			['field' => 'company_name', 'label' => 'Company Name', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
			['field' => 'description', 'label' => 'Description', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
										
		] );
		if( $this->form_validation->run() == FALSE ) :
			$this->data['record']= $this->companies->detail_companies($this->uri->segment(4));
			//print_r($this->data['record']);exit;
			$this->load->view( "templates/header", $this->data );
			$this->load->view( 'company/edit');	
			$this->load->view( "templates/side_menu" );		
			$this->load->view( "templates/footer" );
		else :
		
			if( $this->companies->edit_companies($this->uri->segment(4)) ) : 
				$this->session->set_flashdata( "success","Company info edited successfully" );
				redirect( 'super/company' );
				
			else :
				$this->session->set_flashdata( "error","Error occurs while editing company" );
				redirect( 'super/company/edit' );
			endif;
		endif;	
		
	}

	
	 public function del()
	 {
           $this->load->helper('form');
			if( $this->companies->deletee_companies($this->uri->segment(4)) ) : 
				$this->session->set_flashdata( "message","Company Removed successfully" );
				redirect( 'super/company/' );
			else :
				$this->session->set_flashdata( "message", "Error occurs while deleting Company" );
				redirect( 'super/company/' );
			endif;
			
	
	 } 
	 
	 
	 	public function stations()
	{
        if($this->_is_ajax()){
            $this->load->helper(array('url','form','html'));
            $state = $this->input->get('name', TRUE);
            $data['stations'] = $this->cs->stations($state);
            //echo "State is $state";
            echo json_encode($data);
            //return $data;
        }else{
            echo "Apparently is_ajax returned false!";
            show_error('This method can only be accessed internally.', 404);
        }
	}


	 	public function earnings()
	{
        
            echo "Apparently the page is under maintainance check soon";
          
	}
	
	public function transaction()

	{   
        
		
		//$this->data['name']= $this->companies->company($_SESSION['co_id']);
		//$this->data['records']= $this->orders->receiptlpg();
		//$this->data['name']= $this->dashes->backend();
	    $this->data['companies']= $this->companies->companies();
	    $this->load->view('templates/header', $this->data);
		$this->load->view('transaction/select');
	    $this->load->view('templates/footer');	
				
	}
	
	public function get()

	{   
        $this->load->helper('form');
		$this->load->view('header');
		//$this->data['records']= $this->orders->receiptlpg();
		//$this->data['records']= $this->orders->bidlist();
	    $this->data['records']= $this->cs->orders();
		$this->load->view('su/commons/list', $this->data);
	    $this->load->view('footer');	
				
	}
	
	   function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    } 
	
}