<?php 
if( !defined('BASEPATH') ) exit('No direct script access allowed');

class Product extends CI_Controller 
{


	public function __construct()
	{
  		parent::__construct();
		
		 if (!$this->session->userdata('is_logged_in')) {
            redirect('authentication');
		} 
		
		$this->load->model("products");
		$this->load->model("counties");
		$this->data['parent_page'] = 'Product';
		$this->data['home'] = 'Home';
		$this->_ci =& get_instance();
	

  	}
  	public function index(){
	//	echo 1;exit;

		$this->form_validation->set_rules( [
			['field' => 'product_name', 'label' => 'Product', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
			// [
			// 	'field' => 'corporate_name', 
			// 	'label' => 'Corporate Name', 
			// 	'rules' => 'trim|required|is_unique[facilities.corporate_name]',
			// 	'errors' => ['required' => 'Please enter the %s.','is_unique' => 'This %s is already in use.',],
			// ],
		
										
		] );
		
		if( $this->form_validation->run() == FALSE ) :
			
			$this->data['title'] = "Ordering:: Products";
			$this->data['page'] = 'Product/Add';
			//$this->data['county']=$this->counties->counties();
			$this->data['facility']=$this->products->facilities();
			$this->load->view( "templates/header", $this->data );
			$this->load->view( 'product/add' );		
			$this->load->view( "templates/side_menu" );			
			$this->load->view( "templates/footer" );
		else :
			if($this->products->saves($this->_ci->input->user_agent(),$this->_ci->input->ip_address())){
			 
					$this->session->set_flashdata( "success", "Product created successfully " );
					redirect( 'product' );
				
			}else{

				$this->session->set_flashdata( "error", "An error occurred while creating Product" );
				redirect( 'product' );
			}
			
			
		endif;	
	}

	

}