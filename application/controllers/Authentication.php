<?php 
if( ! defined('BASEPATH') ) exit( 'No direct script access allowed' );

class Authentication extends CI_Controller
{				
	protected $data = array();
	
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('authentications' ); 
		$this->load->library('sms');
		$this->load->library('emails');
		$this->load->model("Users");
	
		
	}
	
	public function index()
	{
		$this->form_validation->set_rules( 'email', 'Username', 'trim|required' );
		$this->form_validation->set_rules( 'password', 'Password', 'trim|required' );
		
		if( $this->form_validation->run() == FALSE ) :
			$this->data['title'] = "Ordering :: Login";
			$this->load->view( "templates/lheader", $this->data );
			$this->load->view( "authentications/login" );
			$this->load->view( "templates/lfooter" );
			 
		else :
		$user = $this->authentications->login();
			if(!empty( $user )){

				if( password_verify( $this->input->post( "password" ), $user->password ) ) {

					$data = array(
						 'is_logged_in' => '1',
						  'admin_id' => $user->id,
						  'givenEmail'=>$user->email,
						  'role_id'=>$user->role_id,
						  'mobile1'=>$user->mobile1,
						  'role_name'=>$user->title,
						  'admin_name'=>$user->first_name .' '. $user->last_name, 
						  'first_name'=>$user->first_name , 
						  'county_id'=>$user->county_id,
						  'subcounty_id'=>$user->sub_county_id,
					);

					

					$this->session->set_userdata( $data );
					//$this->sendotp();

					
					//redirect('authentication/verify');
				
				    redirect('facility/lists');
						
					
				}else{
				
					$this->session->set_flashdata( "loginerror", "You have entered wrong password" );
					redirect( "authentication" );	
					
				}
				
			}else{
				
					$this->session->set_flashdata( "loginerror", " This email ". $this->input->post( "email" ) ." does not exist." );
				    redirect( "authentication" );	
			}
			endif;
			
	}


	public function is_logged()
	{
		if( !$this->authentications->is_logged() )
			redirect('authentication');

	}	
	
	public function logout()
	{
		if( $this->authentications->logout() )
			redirect('authentication');
		else 
			redirect('facility/lists');
			//redirect('authentication');
		
	}

	private function sendemail()
	{
		$message = array();
		$message['email'] = $_SESSION['email'];
		$message['message'] = "Your one time password is ".$_SESSION['otp'];
		$message['name'] = $_SESSION['first_name'];
		$result = $this->emails->sendmail($message);
		return $result;
		


	}
	private function sendsms(){
		$phone = $_SESSION['mobile1'];

		//echo $phone ; exit;
		if (substr($phone, 0,1)=='0'){
			$phone = '254'.substr($phone, 1);
		}
		if(substr($phone, 0,3)=='254')
		{
			$phone = $phone;
		}
		$otp = $_SESSION['otp'];
		$message = "Your one time password is ".$otp .'.     ';
		$result = $this->sms->sendsms($phone, $message);
		if(isset($result)){
			return true;
		}else{
			return false;
		}


	}

	public function sendotp(){
		$data['otp'] = $this->generate_otp();
		$this->session->set_userdata( $data );
		$this->sendsms();
		$this->sendemail();
	}
	public function verify()
	{
		
		$this->form_validation->set_rules( 'otp', 'One Time Pin', 'trim|required' );
		if( $this->form_validation->run() == FALSE ) :
			$this->data['title'] = "Ordering :: OTP";
			$this->load->view( "templates/lheader", $this->data );
			$this->load->view( "authentications/otp" );
			$this->load->view( "templates/lfooter" );
			 
		else :
			if($this->input->post('otp') == $_SESSION['otp']){
				redirect('facility/lists');
			}else{
				$this->session->set_flashdata( "loginerror", "Wrong OTP. Kindly contact admin or Resend OTP" );
				    redirect( "authentication/verify" );

			}
		endif;
	}
	public function setpassword(){
		$this->data['title'] = "Finance :: Reset the password";
		$this->data['page'] = 'Home';
		$this->data['parent_page'] = "Facility";
		$this->data['sub_title'] = "Reset password";
		$this->form_validation->set_rules( 'password', "Password", 'trim|required' );
		$this->form_validation->set_rules( 'newpassword', "New Password", 'trim|required|min_length[5]|max_length[10]' );
		$this->form_validation->set_rules( 'conpassword', "Confirm Password", 'trim|required|matches[newpassword]' );
		
		if($this->form_validation->run()==FALSE):
			$this->load->view( "templates/header", $this->data );
			$this->load->view( 'commons/new_password' );		
			$this->load->view( "templates/footer" );
		else:
			if ($this->Users->get_oldpass()):
					if($this->Users->changepassword()):	
						$this->session->set_userdata('password_set', 1);
						$this->session->set_flashdata( "message",'Password Successfully Changed');
						
						redirect('authentication/logout');
						
					else:
						
						$this->session->set_flashdata( "error", "Error changing Password");
						redirect('authentication/setpassword');
					endif;
			
			else:
				
				$this->session->set_flashdata( "error", "Old password is wrong");
				redirect('authentication/setpassword');
			endif;
		endif;	

	}
	private function generate_otp()
	{
		return mt_rand(1000,9999);
	}

	public function resendotp(){
		$data['otp'] = $this->generate_otp();
		$this->session->set_userdata( $data );
		$this->sendsms();
		$this->sendemail();
		redirect('authentication/verify');
	}

	public function forgot_password(){
		$this->data['title'] = "Ordering :: Forgot password";
		$this->form_validation->set_rules( 'email', "Email", 'trim|required' );
		
		
		if($this->form_validation->run()==FALSE):
			$this->load->view( "templates/lheader", $this->data );
			$this->load->view( 'authentications/forgot' );		
			$this->load->view( "templates/lfooter" );
		else:
			$user = $this->authentications->login();
			if(!empty( $user )){
				$password = $this->generate_otp();
				if($this->authentications->forget_passwords($password,$user->first_name,$user->id)){
					$message = array();
					$message['email'] = $user->email;
					$message['message'] = "Your new password is ". $password;
					$message['name'] = $user->first_name;
					$result = $this->emails->sendmail($message);
					$this->passwordsms($user->mobile1,$password);

					$this->session->set_flashdata( "message", " A new password has been sent to your email or mobile" );
					redirect( "authentication" );
				}else{
					$this->session->set_flashdata( "loginerror", " An error occurred while resetting password .try again" );
					redirect( "authentication/forgot_password" );
				}
				


			}else{
				$this->session->set_flashdata( "loginerror", " This email ". $this->input->post( "email" ) ." does not exist." );
				redirect( "authentication/forgot_password" );	

			}
					
		endif;	

	}
	private function passwordsms($phone,$password){
		//$phone = $phone;

		//echo $phone ; exit;
		if (substr($phone, 0,1)=='0'){
			$phone = '254'.substr($phone, 1);
		}
		if(substr($phone, 0,3)=='254')
		{
			$phone = $phone;
		}
		
		$message = "Your new password is ".$password .'.     ';
		$result = $this->sms->sendsms($phone, $message);
		if(isset($result)){
			return true;
		}else{
			return false;
		}


	}

	
	public function __destruct() 
	{
	
	}	
}