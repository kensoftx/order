<?php 
if( !defined('BASEPATH') ) exit('No direct script access allowed');

class User extends CI_Controller 
{


	public function __construct()
	{
  		parent::__construct();
		
		 if (!$this->session->userdata('is_logged_in')) {
            redirect('authentication');
		} 
		
		
		$this->load->model("users");
        $this->load->model("counties");
		$this->data['parent_page'] = 'User';
		$this->data['home'] = 'Home';
		$this->_ci =& get_instance();
		
		
  	}

  	public function index()
	{

		$this->data['title'] = "Ordering:: User";

		$this->data['page'] = 'User';
	
		$this->data['records']= $this->users->lists();
		$this->load->view( "templates/header", $this->data );
		$this->load->view( 'user/list' );
		$this->load->view( "templates/side_menu" );			
		$this->load->view( "templates/footer" );
	}
	
	public function add()
	{
		$this->form_validation->set_rules( [
			['field' => 'fname', 'label' => 'First name', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
			['field' => 'lname', 'label' => 'Last name', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
			['field' => 'role_id', 'label' => 'Role', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
			[
				'field' => 'email', 
				'label' => 'Email', 
				'rules' => 'trim|required|is_unique[sys_users.email]',
				'errors' => ['required' => 'Please enter the %s.','is_unique' => 'This %s is already in use.',],
			],
			[
				'field' => 'mobile', 
				'label' => 'Mobile No.', 
				'rules' => 'trim|required|is_unique[sys_users.mobile1]',
				'errors' => ['required' => 'Please enter the %s.','is_unique' => 'This %s is already in use.',],
			],
			[
				'field' => 'password', 
				'label' => 'password', 
				'rules' => 'trim|required|min_length[4]',
				'errors' => ['required' => 'Please enter the %s.', 'min_length' => 'The %s must be at least 4 characters.',],
			],
			[	
				'field' => 'password1', 
				'label' => 'Retype Password', 
				'rules' => 'trim|required|matches[password]',
				'errors' => ['required' => 'Please confirm the %s.', 'matches' => 'This %ss do not match.',],
			],									
		] );
		if( $this->form_validation->run() == FALSE ) :
			$this->data['title'] = "Ordering:: Add User";
			$this->data['page'] = 'User/Add';
			$this->data['county']=$this->counties->counties();
			$this->data['subcounty']=$this->counties->sub_counties();
			$this->data['user_roles']=$this->users->roles();
			$this->data['sub_title']="Add";
			$this->load->view( "templates/header", $this->data );
			$this->load->view( 'user/add' );	
			$this->load->view( "templates/side_menu" );	
			$this->load->view( "templates/footer" );
		else :
			if( $this->users->create($this->_ci->input->user_agent(),$this->_ci->input->ip_address()) ) : 
				$this->session->set_flashdata( "success",'User added successfully' );
				redirect( 'user' );
			else :
				$this->session->set_flashdata( "error", 'Error occurred while adding user ' );
				redirect( 'user' );
			endif;
			
		endif;	
		
	}
	public function edit()
	{	
		$this->data['title'] = "Ordering:: Edit User";
		$this->data['page'] = 'User/Edit';
		$this->data['fa']="fa fa-edit";
		$this->form_validation->set_rules( [
			['field' => 'fname', 'label' => 'Firstname', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
			['field' => 'lname', 'label' => 'Lastname', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
			['field' => 'email', 'label' => 'email', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
			['field' => 'role_id', 'label' => 'role', 'rules' => 'trim|required', 'errors' => ['required' => 'Please enter the %s.'],],
										
		] );
		if( $this->form_validation->run() == FALSE ) :
			$this->data['record']= $this->users->detail_user($this->uri->segment(3));
			
			$this->data['user_roles']=$this->users->roles();
			
			$this->load->view( "templates/header", $this->data );
			$this->load->view( 'user/edit' );	
			$this->load->view( "templates/side_menu" );		
			$this->load->view( "templates/footer" );
		else :
		
			if( $this->users->edit_users($this->uri->segment(3)) ) : 
				$this->session->set_flashdata( "success","user info edited successfully" );
				redirect( 'user' );
				
			else :
				
				redirect( 'user' );
			endif;
		endif;	
		
	}
	public function deletes()
	 {

			if( $this->users->deletee($this->uri->segment(3)) ) : 
				$this->session->set_flashdata( "success","User deleted successfully" );
				redirect( 'user' );
			else :
				$this->session->set_flashdata( "error", "Error occurs while deleting user" );
				redirect( 'user' );
			endif;
			
	
	 } 
	 public function activate()
	 {

			if( $this->users->activate_users($this->uri->segment(3)) ) : 
				$this->session->set_flashdata( "success","User activated successfully" );
				redirect( 'user' );
			else :
				$this->session->set_flashdata( "error", "Error occurs while activating user" );
				redirect( 'user' );
			endif;
			
	
	 } 
	 public function deactivate_user()
	 {

			if( $this->users->deletee_users($this->uri->segment(3)) ) : 
				$this->session->set_flashdata( "success","User deactivated successfully" );
				redirect( 'user' );
			else :
				$this->session->set_flashdata( "error", "Error occurs while deactivating user" );
				redirect( 'user' );
			endif;
			
	
	 }
}