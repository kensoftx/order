<div class="section-body">
    <div class="row">

            <center>
                    
                         
                                    
                        
                          <?php if( $this->session->flashdata('error') != "" ) : ?>
                           <div class="row"><div class="col-xs-12"><div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div></div>
                         </div>
                        <?php endif; ?>
                        <?php if( $this->session->flashdata('success') != "" ) : ?>
                           <div class="row"><div class="col-xs-12"><div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div></div>
                         </div>
                        <?php endif; ?>
              </center>
                 
        <div class="col-md-12">
            <div class="panel panel-default">

                     <?php echo form_open( 'super/company/edit',array('role'=>'','data-toggle'=>"" ,'class'=>"form")) ; ?>
                                 
                        <div class="card-head style-primary ">
                            <header>Update Company</header>
                        </div>
                            <div class="card-body floating-label">

                                <div class="col-md-12">
                                   
                                    
                                         <div class="box-body">
                                        <div class="row">   
                                              <div class="col-md-3">  
                                              </div>
                                                   
                                                <div class="col-md-6"> 
                                                    <div class="form-group has-feedback">
                                                        <input type="text" class="form-control" placeholder="Enter Company Name" required='true' name='company_name' value="<?php echo $record->name; ?>">
                                                        <label for="Company"> Enter Company Name  <span style="color:red; font-size: 80%"> * </span></label>
                                                        
                                                    </div>
                                                   <span style="color:red; font-size: 80%"><?php echo form_error('company_name'); ?></span>
                                                    
                                                </div>
                                              <div class="col-md-3">  
                                            </div>
                                               
                                              
                                            </div>

                                            <div class="row">   
                                              <div class="col-md-3">  
                                              </div>
                                                   
                                                <div class="col-md-6"> 
                                                    <div class="form-group has-feedback">
                                                        <textarea type="text" class="form-control" placeholder="Enter Description" required='true' name='description' value="<?php echo $record->descr; ?>"></textarea>
                                                        <label for="Company"> Enter Description  <span style="color:red; font-size: 80%"> * </span></label>
                                                        
                                                    </div>
                                                   <span style="color:red; font-size: 80%"><?php echo form_error('description'); ?></span>
                                                    
                                                </div>
                                              <div class="col-md-3">  
                                            </div>
                                               
                                              
                                            </div>
                                          </div>
                                
                                 </div>
                        
                           
                                           
                                  
                                        </div><!--end .card-body -->
                                         <div class="panel-footer">
                                          <div class="row"> 
                                          <div class="col-md-3">
                                          </div>
                                          <div class="col-md-6">

                                                    <a href="<?php echo  base_url('super/company')?>"><button type="button" class="btn btn-warning">Back</button></a>                                    
                                                    <button type="submit" class="btn btn-primary pull-right">Edit Company</button>
                                            </div>
                                          <div class="col-md-3">
                                          </div>
                                        </div>
                                         </div>
                                       
                                      
                                            
                                      
                                    
                                </form>
                            </div><!--end .col -->
                        </div>
             </div><!--end .row -->


</div>

