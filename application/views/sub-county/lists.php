
            <center>
                 <?php if( $this->session->flashdata('error') != "" ) : ?>
                   <div class="row"><div class="col-xs-12"><div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div></div></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('success') != "" ) : ?>
                   <div class="row"><div class="col-xs-12"><div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div></div></div>
                <?php endif; ?>
            </center>
  <div class="row">
         <div class="col-md-12">
            <div class="card" style="display: block;overflow-x: auto; white-space: nowrap;">
            <!-- <div><a class="btn btn-primary btn-flat" href="#"><i class="fa fa-lg fa-plus"></i></a></div>-->
              <div class="card-body" >
              
                <table id="datatable1" class="table table-striped table-hover">
              
               <thead>
                    <tr>
                      <th>#</th>
                      <th>County</th>
                        <th>Sub-County</th>
                        <th>Date Created</th>
                       <!-- <th width="170px">Action</th> -->
                    </tr>
                </thead>
                
                <tbody>
                <?php $total=0;?>
                <?php for( $i=0; $i<count( $records ); $i++ ) : ?>
                  <?php $record = &$records[$i]; ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td><?php echo  ucwords(strtolower($record->county));?></td>
                    <td><?php echo  ucwords(strtolower($record->name));?></td>
                    <td><?php echo  ucwords(strtolower($record->date));?></td>

                    
                    
                       <!--  <small>
                        
                        <td>
                            <?php if($record->status=="1") { ?>
                        <a href="<?php echo  base_url('super/county/deactivate/'.$record->id );?>" title="Deactivate <?php echo  ucwords(strtolower($record->name ) ); ?>" data-toggle="tooltip" class='btn btn-primary btn-xs pull-left' style="margin-left:1px;"><i aria-hidden="true" onclick=" return confirm('Do you want to deactivate this county?')"></i>Deactivate </a>&nbsp;
                        <a href="<?php echo  base_url('super/county/edit/'.$record->id );?>" title="Edit <?php echo  ucwords(strtolower($record->name) ); ?>" data-toggle="tooltip" class='btn ink-reaction btn-raised btn-warning btn-xs pull-center' style="margin-left:1px;"><i aria-hidden="true" onclick=" return confirm('Do you want to edit county')"></i>Edit </a>

                     
                       
                      <?php }else { ?>
                        
                        
                        <a href="<?php echo  base_url('super/county/activate/'.$record->id );?>" title="Activate <?php echo  ucwords(strtolower($record->name ) );  ?>" data-toggle="tooltip" class='btn btn-success btn-xs pull-left' style="margin-left:1px;"><i  aria-hidden="true"></i> Activate </a>&nbsp;
                        <a href="<?php echo  base_url('super/county/edit/'.$record->id );?>" title="Edit <?php echo  ucwords(strtolower($record->name) ); ?>" data-toggle="tooltip" class='btn ink-reaction btn-raised btn-warning btn-xs pull-center' style="margin-left:1px;"><i aria-hidden="true" onclick=" return confirm('Do you want to edit county')"></i>Edit </a>
                    
                  
                     
                    </small>
                      <?php } ?>
                    </td>  -->
                </tr>
                <?php endfor; ?>
                
                </tbody>
                
              </table>
                
          </div>
        
        </div>
      </div>  
      
 </div>      