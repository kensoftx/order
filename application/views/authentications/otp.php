 
     <!-- BEGIN LOGIN SECTION -->
        <section class="section-account">
            <div class="img-backdrop" style="background-image: url('<?php echo base_url();?>assets/img/img16.jpg')"></div>
            <div class="spacer"></div>
            <div class="card contain-sm style-transparent">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-7">
                            <br/>
                            <center>
                                
                                  <span class="text-lg text-bold text-primary">Ordering Portal</span>

                                  <?php if( $this->session->flashdata('loginerror') != "" ) : ?>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="toast-error" class="alert alert-danger">
                                                <?php echo $this->session->flashdata('loginerror'); ?>
                                                    
                                                </div>
                                        </div>
                                    </div>
                                <?php endif; ?> 
                                <?php if( $this->session->flashdata('message') != "" ) : ?>
                                    <div class="row"><div class="col-lg-12"><div class="alert alert-danger"><?php echo $this->session->flashdata('message'); ?></div></div>
                                <?php endif; ?>       
                            </center>
                          
                            <br/>
                            <?php echo form_open( 'authentication/verify',array('role'=>'form','data-toggle'=>"validator" ,'class'=>"form floating-label",'accept-charset'=>"utf-8")) ; ?> 
                            
                                <div class="form-group">
                                    <input type="number" class="form-control" id="otp" name="otp" required>
                                    <label for="Email">Enter the one time pin sent to your email or phone</label>
                                     <p class="help-block"><a href="<?php echo base_url('authentication/resendotp');?>">Resent OTP?</a></p>
                                </div>
                                
                                <br/>
                                <div class="row">
                                  
                                    <div class="col-xs-7 text-right">
                                        <button class="btn btn-primary btn-raised" type="submit">Verify</button>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                            </form>
                        </div><!--end .col -->
                       
                            </div><!--end .row -->
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </section>
                <!-- END LOGIN SECTION -->

        







  