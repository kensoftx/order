
        <!-- BEGIN LOGIN SECTION -->
        <section class="section-account">
            <div class="img-backdrop" style="background-image: url('<?php echo base_url();?>assets/img/img16.png')"></div>
            <div class="spacer"></div>
            <div class="card contain-sm style-transparent">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            <br/>
                            <center>
                                
                                  <span class="text-lg text-bold text-danger">Ordering Portal</span>

                                  <?php if( $this->session->flashdata('loginerror') != "" ) : ?>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="toast-error" class="alert alert-danger">
                                                <?php echo $this->session->flashdata('loginerror'); ?>
                                                    
                                                </div>
                                        </div>
                                    </div>
                                <?php endif; ?> 
                                <?php if( $this->session->flashdata('message') != "" ) : ?>
                                    <div class="row"><div class="col-lg-12"><div class="alert alert-success"><?php echo $this->session->flashdata('message'); ?></div></div>
                                </div>
                                <?php endif; ?>       
                            </center>
                          
                            <br/>
                            <?php echo form_open( 'authentication',array('role'=>'form','data-toggle'=>"validator" ,'class'=>"form floating-label",'accept-charset'=>"utf-8")) ; ?> 
                            
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" required>
                                    <label for="Email">Username</label>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" name="password" required>
                                    <label for="password">Password</label>
                                    <p class="help-block"><a href="<?php echo base_url('authentication/forgot_password');?>">Forgotten?</a></p>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-xs-6 text-left">
                                        <div class="checkbox checkbox-inline checkbox-styled">
                                            <label>
                                                <input type="checkbox"> <span>Remember me</span>
                                            </label>
                                        </div>
                                    </div><!--end .col -->
                                    <div class="col-xs-6 text-right">
                                        <button class="btn btn-primary btn-raised" type="submit">Login</button>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                            </form>
                        </div><!--end .col -->
                       
                            </div><!--end .row -->
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </section>
                <!-- END LOGIN SECTION -->

        

