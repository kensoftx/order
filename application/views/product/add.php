<div class="section-body">
    <div class="row">

            <center>
                    
                         
                                    
                        
                          <?php if( $this->session->flashdata('error') != "" ) : ?>
                           <div class="row"><div class="col-xs-12"><div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div></div>
                         </div>
                        <?php endif; ?>
                        <?php if( $this->session->flashdata('success') != "" ) : ?>
                           <div class="row"><div class="col-xs-12"><div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div></div>
                         </div>
                        <?php endif; ?>
              </center>
                 
        <div class="col-md-12">
            <div class="panel panel-default">

                     <?php echo form_open( 'product',array('role'=>'','data-toggle'=>"" ,'class'=>"form")) ; ?>
                                 
                        <div class="card-head style-primary ">
                            <header>Create Product</header>
                        </div>
                            <div class="card-body floating-label">

                                <div class="col-md-12">
                                   
                                    
                                         <div class="box-body">
                                        <div class="row">   
                                              <div class="col-md-3">  
                                              </div>
                                                   
                                                <div class="col-md-6"> 
                                                    <div class="form-group has-feedback">
                                                        <input type="text" class="form-control" placeholder="Enter Product" required='true' name='product_name' value="<?php echo set_value('product_name'); ?>">
                                                        <label for="Product"> Enter Product   <span style="color:red; font-size: 80%"> * </span></label>
                                                        
                                                    </div>
                                                   <span style="color:red; font-size: 80%"><?php echo form_error('product_name'); ?></span>
                                                    
                                                </div>
                                              <div class="col-md-3">  
                                            </div>
                                        </div>
                                         <div class="row">   
                                              <div class="col-md-3">  
                                              </div>
                                                   
                                                <div class="col-md-6"> 
                                                    <div class="form-group has-feedback">
                                                       <select  name="facility" id="country" class="form-control select2-list" data-placeholder="" required >
                                                            <option value=""> Select Facility  </option> <span style="color:red; font-size: 80%"> * </span>
                                                                <?php foreach( $facility as $count ) : ?>
                                                                    <option value="<?php echo $count->id; ?>"><?php echo $count->name; ?></option>
                                                                <?php endforeach; ?>
                                                                </select> 
                                                        <label for="county"> </label>
                                                              
                                                        
                                                        </div>
                                                        <br><span style="color:red; font-size: 80%"><?php echo form_error('facility'); ?></span>
                                                    
                                                </div>
                                              <div class="col-md-3">  
                                            </div>
                                        </div>
                                     
                                          </div>
                                
                                 </div>
                        
                           
                                           
                                  
                                        </div><!--end .card-body -->
                                         <div class="panel-footer">
                                          <div class="row"> 
                                          <div class="col-md-3">
                                          </div>
                                          <div class="col-md-6">

                                                    <a href="<?php echo  base_url('facility/lists')?>"><button type="button" class="btn btn-default">Back</button></a>                                    
                                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                            </div>
                                          <div class="col-md-3">
                                          </div>
                                        </div>
                                         </div>
                                       
                                

                                            
                                                
                                                
                                            
                                      
                                    
                                </form>
                            </div><!--end .col -->
                        </div>
             </div><!--end .row -->


</div>

