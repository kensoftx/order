
            <center>
                 <?php if( $this->session->flashdata('error') != "" ) : ?>
                   <div class="row"><div class="col-xs-12"><div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div></div></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('success') != "" ) : ?>
                   <div class="row"><div class="col-xs-12"><div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div></div></div>
                <?php endif; ?>
            </center>
  <div class="row">
         <div class="col-md-12">
            <div class="card" style="display: block;overflow-x: auto; white-space: nowrap;">
            <!-- <div><a class="btn btn-primary btn-flat" href="#"><i class="fa fa-lg fa-plus"></i></a></div>-->
              <div class="card-body" >
              
                <table id="datatable1" class="table table-striped table-hover">
              
               <thead>
                    <tr>
                      <th>#</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Period</th>
                       <th width="170px">Action</th>
                    </tr>
                </thead>
                
                <tbody>
                <?php $total=0;?>
                <?php for( $i=0; $i<count( $records ); $i++ ) : ?>
                  <?php $record = &$records[$i]; ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td><?php echo  ucwords(strtolower($record->name));?></td>
                    <td><?php echo  ucwords($record->code);?></td>
                    <td><?php echo  ucwords(strtolower($record->date_created));?></td>

                    
                    
                        <small>
                        <td>
                            <?php if($record->status=="1") { ?>
                        <a href="<?php echo  base_url('facility/view/'.$record->id );?>" title="View <?php echo  ucwords(strtolower($record->name ) ); ?>" data-toggle="tooltip" class='btn btn-primary btn-xs pull-left' style="margin-left:1px;"><i aria-hidden="true" onclick=" return confirm('Are you sure to view facility')"></i>View </a>

                     
                       
                      <?php }else { ?>
                        
                        
                        <a href="#" title="No data <?php echo  ucwords(strtolower($record->name ) );  ?>" data-toggle="tooltip" class='btn btn-success btn-xs pull-left' style="margin-left:1px;"><i  aria-hidden="true"></i> No details </a>
                    
                  
                     
                    </small>
                      <?php } ?>
                    </td> 
                </tr>
                <?php endfor; ?>
                
                </tbody>
                
              </table>
                
          </div>
        
        </div>
      </div>  
      
 </div>      