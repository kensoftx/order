<div class="section-body">
    <div class="row">
                 
        <div class="col-md-12">
            <div class="panel panel-default">

                     <?php echo form_open( 'user/add',array('role'=>'','data-toggle'=>"" ,'class'=>"form")) ; ?>
                                 
                        <div class="card-head style-primary ">
                            <header>Create an account</header>
                        </div>
                            <div class="card-body floating-label">

                                <div class="col-md-6">
                                   
                                    <div class="panel-footer">
                                        User Details
                                       
                                    </div>
                                         <div class="box-body">
                                        <div class="row">    
                                                   
                                                <div class="col-md-6"> 
                                                    <div class="form-group has-feedback">
                                                        <input type="text" class="form-control" placeholder="First name" required='true' name='fname' value="<?php echo set_value('fname'); ?>">
                                                        <label for="Firstname2">First name  <span style="color:red; font-size: 80%"> * </span></label>
                                                        
                                                    </div>
                                                   <span style="color:red; font-size: 80%"><?php echo form_error('fname'); ?></span>
                                                    
                                                </div>
                                                 <div class="col-md-6"> 
                                                     <div class="form-group has-feedback">
                                                         <input type="text" class="form-control" placeholder="Last name" required='true' name='lname' value="<?php echo set_value('lname'); ?>">
                                                      <label for="last">Last name <span style="color:red; font-size: 80%"> * </span>
                                                      </div></label> 
                                                     <span style="color:red; font-size: 80%"><?php echo form_error('lname'); ?></span>
                                                    </div>
                                                                    
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6"> 
                                                     <div class="form-group">
                                                        <select  name="role_id" class="form-control select2-list" data-placeholder="" required >
                                                            <option value=""> Select Role  </option> <span style="color:red; font-size: 80%"> * </span>
                                                                <?php foreach( $user_roles as $user_role ) : ?>
                                                                    <option value="<?php echo $user_role->id; ?>"><?php echo $user_role->title; ?></option>
                                                                <?php endforeach; ?>
                                                                </select> 
                                                        <label for="role"> </label>
                                                              
                                                        
                                                        </div>
                                                        <br><span style="color:red; font-size: 80%"><?php echo form_error('role_id'); ?></span>
                                                  </div>
                                                 <div class="col-md-6"> 
                                                     <div class="form-group">
                                                        <input type="telephone" class="form-control" placeholder="Mobile No" required='true' name='mobile' value="<?php echo set_value('mobile'); ?>">
                                                      <label for="mobile">Mobile No <span style="color:red; font-size: 80%"> * </span></label> 
                                                        
                                                        
                                                        <br><span style="color:red; font-size: 80%"><?php echo form_error('mobile'); ?></span>
                                                      </div>
                                                    </div>

                                                  </div>

                                                   <div class="row">
                                                <div class="col-md-6"> 
                                                     <div class="form-group">
                                                        <select  name="county_id" class="form-control select2-list" data-placeholder="" required >
                                                            <option value=""> Select County  </option> <span style="color:red; font-size: 80%"> * </span>
                                                                <?php foreach( $county as $count ) : ?>
                                                                    <option value="<?php echo $count->id; ?>"><?php echo $count->name; ?></option>
                                                                <?php endforeach; ?>
                                                                </select> 
                                                        <label for="role"> </label>
                                                              
                                                        
                                                        </div>
                                                        <br><span style="color:red; font-size: 80%"><?php echo form_error('county_id'); ?></span>
                                                  </div>
                                                  <div class="col-md-6"> 
                                                     <div class="form-group">
                                                        <select  name="subcounty_id" class="form-control select2-list" data-placeholder="" required >
                                                            <option value=""> Select Sub-County  </option> <span style="color:red; font-size: 80%"> * </span>
                                                                <?php foreach( $subcounty as $sub ) : ?>
                                                                    <option value="<?php echo $sub->id; ?>"><?php echo $sub->name; ?></option>
                                                                <?php endforeach; ?>
                                                                </select> 
                                                        <label for="role"> </label>
                                                              
                                                        
                                                        </div>
                                                        <br><span style="color:red; font-size: 80%"><?php echo form_error('subcounty_id'); ?></span>
                                                  </div>

                                                  </div>
                                              
                                                </div>
                                
                                 </div>
                            
                             <div class="col-md-6">
                                    
                                 
                                       <div class="panel-footer">
                                      Login Details
                                     
                                   </div>
                                        <div class="box-body">
                                          <div class="row">
                                            <div class="col-md-6">
                                                 
                                                     <div class="form-group has-feedback">
                                                        <input type="email" class="form-control" placeholder="Email" required='true' name='email' value="<?php echo set_value('email'); ?>">
                                                        <label for="email">Email <span style="color:red; font-size: 80%"> * </span></label>
                                                            
                                                            
                                                            
                                                        </div>
                                                        <span style="color:red; font-size: 80%"><?php echo form_error('email'); ?></span>
                                                 
                                              </div>
                                                            
                                             <div class="col-md-6"> 
                                                      <div class="form-group has-feedback">
                                                         <input type="password" class="form-control" placeholder="Password" required="true" name='password'>
                                                       <label for="county">Password <span style="color:red; font-size: 80%"> * </span></label> 
                                                       
                                                        
                                                      </div>
                                                      <span style="color:red; font-size: 80%"><?php echo form_error('password'); ?></span>
                                                </div>
                                                 <div class="col-md-6"> 
                                                      <div class="form-group has-feedback"> 
                                                        <input type="password" class="form-control" placeholder="Retype password" required='true' name='password1'>
                                                       <label for="county">Retype password <span style="color:red; font-size: 80%"> * </span>
                                                        </label>
                                                      </div>
                                                      <span style="color:red; font-size: 80%"><?php echo form_error('password1'); ?></span>
                                                  </div>
                                                  
                                            </div>  
                                            </div>
                                             
                                   
                                                        
                              
                                 </div>
                           
                                           
                                  
                                        </div><!--end .card-body -->
                                         <div class="panel-footer">
                                                    <a href="<?php echo  base_url('user')?>"><button type="button" class="btn btn-default">Back</button></a>                                    
                                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                                </div>
                                       
                                

                                            
                                                
                                                
                                            
                                      
                                    
                                </form>
                            </div><!--end .col -->
                        </div>
             </div><!--end .row -->


</div>

