
            <center>
                 <?php if( $this->session->flashdata('error') != "" ) : ?>
                   <div class="row"><div class="col-xs-12"><div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div></div></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('success') != "" ) : ?>
                   <div class="row"><div class="col-xs-12"><div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div></div></div>
                <?php endif; ?>
            </center>
  <div class="row">
         <div class="col-md-12">
            <div class="card" style="display: block;overflow-x: auto; white-space: nowrap;">
            <!-- <div><a class="btn btn-primary btn-flat" href="#"><i class="fa fa-lg fa-plus"></i></a></div>-->
              <div class="card-body" >
              
                <table id="datatable1" class="table table-striped table-hover">
                <thead>
               <tr>
                  <th>#</th>
                  
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Mobile No.</th>
                  <th>Email</th>
                  <th>Role</th>
                  <th>Date Created</th>
                  <th>Action</th>
                 <th>Action</th> 
                </tr>
                </thead>
                <tbody>
                <?php for( $i=0; $i<count( $records ); $i++ ) : ?>
                  <?php $record = &$records[$i]; ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td><?php echo ucfirst($record->first_name);?></td>
                    <td><?php echo ucfirst($record->last_name);?></td>
                    <td><?php echo $record->mobile1;?></td>
                    <td><?php echo $record->email;?></td>
                    <td><?php echo $record->role;?></td>
                    <td><?php echo $record->created_on;?></td>
                    
                    <?php if($record->account_status=="1") { ?>
                    
                        <small>
                        
                     <td> 
                        <a href="<?php echo  base_url('user/edit/'.$record->id );?>" title="Edit <?php echo  ucwords(strtolower($record->first_name .' '. $record->last_name ) ); ?>" data-toggle="tooltip" class='btn ink-reaction btn-raised btn-success pull-left' style="margin-left:1px;"><i aria-hidden="true" onclick=" return confirm('Are you sure to Edit user')"></i>Edit </a>

                       </td> 
                       <td>
                        <a href="<?php echo  base_url('user/deactivate_user/'.$record->id );?>" title="Deactivate <?php echo  ucwords(strtolower($record->first_name .' '. $record->last_name ) ); ?>" data-toggle="tooltip" class='btn ink-reaction btn-raised btn-danger pull-right' style="margin-left:1px;"><i aria-hidden="true" onclick=" return confirm('Are you sure to Deactivate user')"></i>Deactivate </a>
                    </td>
                       
                      <?php }else { ?>
                        <td>
                        <a href="<?php echo  base_url('user/edit/'.$record->id );?>" title="Edit <?php echo  ucwords(strtolower($record->first_name .' '. $record->last_name ) ); ?>" data-toggle="tooltip" class='btn ink-reaction btn-raised btn-success pull-left' style="margin-left:1px;"><i aria-hidden="true" onclick=" return confirm('Are you sure to Edit user')"></i>Edit </a>
                         </td>
                     <td>

                        <a href="<?php echo  base_url('user/activate/'.$record->id );?>" title="Activate <?php echo  ucwords(strtolower($record->first_name .' '. $record->last_name ) );  ?>" data-toggle="tooltip" class='btn ink-reaction btn-raised btn-info pull-right' style="margin-left:1px;"><i  aria-hidden="true"></i> Activate </a></td> 
                   
                    </small>
                      <?php } ?>
                </tr>
                <?php endfor; ?>
                
                </tbody>
                
              </table>
                
          </div>
        
        </div>
      </div>  
      
 </div>      