<div class="section-body">
    <div class="row">
                 
        <div class="col-md-12">
            <div class="panel panel-default">

                     <?php echo form_open( 'user/edit/'. $record->id,array('role'=>'','data-toggle'=>"" ,'class'=>"form")) ; ?>
                                 
                        <div class="card-head style-primary ">
                            <header>Update an account</header>
                        </div>
                            <div class="card-body floating-label">

                                <div class="col-md-6">
                                   
                                    <div class="panel-footer">
                                        User Details
                                       
                                    </div>
                                         <div class="box-body">
                                        <div class="row">    
                                                   
                                                <div class="col-md-6"> 
                                                    <div class="form-group has-feedback">
                                                        <input type="text" class="form-control" placeholder="First name" required='true' name='fname' value="<?php echo $record->first_name; ?>">
                                                        <label for="Firstname2">First name  <span style="color:red; font-size: 80%"> * </span></label>
                                                        
                                                    </div>
                                                   <span style="color:red; font-size: 80%"><?php echo form_error('fname'); ?></span>
                                                    
                                                </div>
                                                 <div class="col-md-6"> 
                                                     <div class="form-group has-feedback">
                                                         <input type="text" class="form-control" placeholder="Last name" required='true' name='lname' value="<?php echo $record->last_name; ?>">
                                                      <label for="county">Last name <span style="color:red; font-size: 80%"> * </span>
                                                      </div></label> 
                                                     <span style="color:red; font-size: 80%"><?php echo form_error('lname'); ?></span>
                                                    </div>
                                                                    
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6"> 
                                                     <div class="form-group">
                                                        <select  name="role_id" class="form-control select2-list" data-placeholder="" required >
                                                            <option value=""> Select Role  </option> <span style="color:red; font-size: 80%"> * </span>
                                                              

                                                                <?php for( $i=0; $i<count( $user_roles ); $i++ ) : ?>
                                                                    <?php $make = &$user_roles[$i]; ?>                                                          
                                                                    <?php $selected = ( $make->id == $record->role_id ) ? "selected=\"selected\"" : ""; ?>                          
                                                                    <option value="<?php echo $make->id; ?>" <?php echo $selected; ?>><?php echo $make->title; ?></option>
                                                                    
                                                                <?php endfor; ?>
                                                                </select> 
                                                        <label for="county"> </label>
                                                              
                                                        
                                                        </div>
                                                        <br><span style="color:red; font-size: 80%"><?php echo form_error('role_id'); ?></span>
                                                  </div>
                                                 <div class="col-md-6"> 
                                                     <div class="form-group">
                                                        <input type="telephone" class="form-control" placeholder="Mobile No" required='true' name='mobile'  value="<?php echo $record->mobile1; ?>">
                                                      <label for="county">Mobile No <span style="color:red; font-size: 80%"> * </span></label> 
                                                        
                                                        
                                                        <br><span style="color:red; font-size: 80%"><?php echo form_error('mobile'); ?></span>
                                                      </div>
                                                    </div>

                                                  </div>
                                              
                                                </div>
                                
                                 </div>
                            
                                       <div class="col-md-6">
                                    
                                 
                                       <div class="panel-footer">
                                      Login Details
                                     
                                      </div>
                                        <div class="box-body">
                                          <div class="row">
                                            <div class="col-md-8">
                                                 
                                                     <div class="form-group has-feedback">
                                                        <input type="email" class="form-control" placeholder="Email" required='true' name='email' value="<?php echo $record->email; ?>">
                                                        <label for="email">Email <span style="color:red; font-size: 80%"> * </span></label>
                                                            
                                                            
                                                            
                                                        </div>
                                                        <span style="color:red; font-size: 80%"><?php echo form_error('email'); ?></span>
                                                 
                                              </div>
                                                            
                                         
                                            </div>  
                                            </div>
                                         </div>
                           
                                           
                                  
                                        </div><!--end .card-body -->
                                         <div class="panel-footer">
                                                    <a href="<?php echo  base_url('user')?>"><button type="button" class="btn btn-default">Back</button></a>                                    
                                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                                </div>
                                       
                                

                                            
                                                
                                                
                                            
                                      
                                    
                                </form>
                            </div><!--end .col -->
                        </div>
             </div><!--end .row -->


</div>


























