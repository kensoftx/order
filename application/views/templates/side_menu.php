				</section>

				<!-- BEGIN BLANK SECTION -->
			</div><!--end #content-->
			<!-- END CONTENT -->

			<!-- BEGIN MENUBAR-->
			<div id="menubar" class="menubar-inverse ">
				<div class="menubar-fixed-panel">
					<div>
						<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
							<i class="fa fa-bars"></i>
						</a>
					</div>
					<div class="expanded">
						<a href="<?php echo base_url('facility/lists')?>"  <?php echo ($parent_page=='Facility')? "class='active'" : "" ?> > class="title">
							<span class="text-lg text-bold text-primary ">Ordering&nbsp;Portal</span>
						</a>
					</div>
				</div>
				<div class="menubar-scroll-panel">
					<!-- BEGIN MAIN MENU -->
					<ul id="main-menu" class="gui-controls">

						<!-- BEGIN DASHBOARD -->
						
						<!-- <li>
							<a href="<?php echo base_url('dashboard')?>" <?php echo ($parent_page=='Dashboard')? "class='active'" : "" ?> >
								<div class="gui-icon"><i class="md md-home"></i></div>
								<span class="title">Dashboard</span>
							</a>
						</li> --><!--end /menu-li -->
						<!-- END DASHBOARD -->

						<!-- BEGIN EMAIL -->

						<!--#################################################################################
								
								Super Admin

						    #################################################################################-->

						<?php if($_SESSION['role_id']==1){ ?>

						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-account-balance"></i></div>
								<span class="title">Counties</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="<?php echo base_url('super/county/add')?>"  <?php echo ($parent_page=='County')? "class='active'" : "" ?>><span class="title">Add County</span></a></li>
								<li><a href="<?php echo base_url('super/county')?>"  <?php echo ($parent_page=='County')? "class='active'" : "" ?> ><span class="title">County List</span></a></li>
								
							</ul><!--end /submenu -->

							<!--start submenu -->
							<ul>
								<li><a href="<?php echo base_url('super/county/add_sub_county')?>"  <?php echo ($parent_page=='County')? "class='active'" : "" ?>><span class="title">Add Sub-County</span></a></li>
								<li><a href="<?php echo base_url('super/county/sub_county')?>"  <?php echo ($parent_page=='County')? "class='active'" : "" ?> ><span class="title">Sub-County List</span></a></li>
								
							</ul><!--end /submenu -->

						</li><!--end /menu-li -->

						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-group-work"></i></div>
								<span class="title">Facilities</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="<?php echo base_url('facility/lists')?>"  <?php echo ($parent_page=='Facility')? "class='active'" : "" ?> ><span class="title">Lists</span></a></li>
								<li><a href="<?php echo base_url('facility')?>"  <?php echo ($parent_page=='Facility')? "class='active'" : "" ?>><span class="title">Add Facility </span></a></li>
							
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->
						
						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-group-work"></i></div>
								<span class="title">Products</span>
							</a>
							<!--start submenu -->
							<ul>
								
								<li><a href="<?php echo base_url('product')?>"  <?php echo ($parent_page=='Product')? "class='active'" : "" ?>><span class="title">Add Product </span></a></li>
							
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->
						
						
						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-account-child"></i></div>
								<span class="title">System Users</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="<?php echo base_url('user')?>"  <?php echo ($parent_page=='User')? "class='active'" : "" ?> ><span class="title">Lists</span></a></li>
								<li><a href="<?php echo base_url('user/add')?>"  <?php echo ($parent_page=='User')? "class='active'" : "" ?>><span class="title">Add User</span></a></li>
								
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->

						<!--#################################################################################
								
								Admin

						    #################################################################################-->

						<?php } elseif($_SESSION['role_id']==2){ ?>

						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-account-balance"></i></div>
								<span class="title">Station</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="<?php echo base_url('station')?>"  <?php echo ($parent_page=='Station')? "class='active'" : "" ?> ><span class="title">Lists</span></a></li>
								<li><a href="<?php echo base_url('station/add')?>"  <?php echo ($parent_page=='Station')? "class='active'" : "" ?>><span class="title">Add Station</span></a></li>
								
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->
						

						<!--#################################################################################
								
								County

						    #################################################################################-->
                       <?php } elseif($_SESSION['role_id']==3){ ?>

						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
								<span class="title">Forecourt Managment</span>
							</a>
							<!--start submenu -->
							<ul>
								
								<li><a href="#"><span class="title">New Order(s)</span></a></li>
								<li><a href="#"><span class="title">Pending Order(s)</span></a></li>
								<li><a href="#"><span class="title">Approved Order(s)</span></a></li>
								<li><a href="#"><span class="title">In Transit Order(s)</span></a></li>
								<li><a href="#"><span class="title">Delivered Order(s)</span></a></li>
								<li><a href="#"><span class="title">Rejected Order(s)</span></a></li>
								
								
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->

						<!-- END LEVELS -->
						
						


						<!--#################################################################################
								
								Sub-County

						    #################################################################################-->

						<?php } else{ ?>

                              
					<?php } ?>

























						<!-- END EMAIL -->

						<!-- BEGIN DASHBOARD -->
						

					</ul><!--end .main-menu -->
					<!-- END MAIN MENU -->

					<div class="menubar-foot-panel">
						<small class="no-linebreak hidden-folded">
							<span class="opacity-75">Copyright &copy; <?php echo date('Y');?></span> <strong>powered by Virscom</strong>
						</small>
					</div>
				</div><!--end .menubar-scroll-panel-->
			</div><!--end #menubar-->
			<!-- END MENUBAR -->