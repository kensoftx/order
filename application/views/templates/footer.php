
        </div><!--end #base-->
        <!-- END BASE -->

        <!-- BEGIN JAVASCRIPT -->
        <script src="<?php echo base_url();?>assets/js/libs/jquery/jquery-1.11.2.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
         <script src="<?php echo base_url();?>assets/js/libs/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/bootstrap/bootstrap.min.js"></script>
          <script src="<?php echo base_url();?>assets/js/libs/spin.js/spin.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/autosize/jquery.autosize.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/select2/select2.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/multi-select/jquery.multi-select.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/spin.js/spin.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
        <script src=".<?php echo base_url();?>assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
       
        <script src="<?php echo base_url();?>assets/js/libs/autosize/jquery.autosize.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/core/source/App.js"></script>
        <script src="<?php echo base_url();?>assets/js/core/source/AppNavigation.js"></script>
        <script src="<?php echo base_url();?>assets/js/core/source/AppOffcanvas.js"></script>
        <script src="<?php echo base_url();?>assets/js/core/source/AppCard.js"></script>
        <script src="<?php echo base_url();?>assets/js/core/source/AppForm.js"></script>
        <script src="<?php echo base_url();?>assets/js/core/source/AppNavSearch.js"></script>
        <script src="<?php echo base_url();?>assets/js/core/source/AppVendor.js"></script>
        <script src="<?php echo base_url();?>assets/js/core/demo/Demo.js"></script>
        <script src="<?php echo base_url();?>assets/js/core/demo/DemoTableDynamic.js"></script>
        <script src="<?php echo base_url();?>assets/js/core/demo/DemoFormComponents.js"></script>
        <!-- END JAVASCRIPT -->

         <script>
        jQuery(document).ready(function() {
    
        "use strict";
        
        jQuery("#customer_id").change(function(){    
          jQuery.ajax({url:"<?php echo base_url(); ?>credit/get_name/"+$(this).val(), success:function(result){
            jQuery("#models_options").html(result);
            var newOptions = JSON.parse(result);          
            var $select = $("#name_id");
            $select.empty(); // remove old options
            $.each( newOptions, function( value, key ) {
              $select.append($("<option></option>").attr( "value", value ).text(key));
            });           
          }});
        });
          });
        </script>
         <script>
        jQuery("#customer_id").change(function(){    
          jQuery.ajax({url:"<?php echo base_url(); ?>credit/get_credit_limit/"+$(this).val(), success:function(result){
            jQuery("#models_options").html(result);
            var newOptions = JSON.parse(result);          
            var $select = $("#credit_id");
            $select.empty(); // remove old options
            $.each( newOptions, function( value, key ) {
              $select.append($("<option></option>").attr( "value", value ).text(key));
            });           
          }});
        });
         
        </script>

       <script>
        jQuery(document).ready(function() {
    
        "use strict";
        
        jQuery("#customer_id").change(function(){    
          jQuery.ajax({url:"<?php echo base_url(); ?>credit/get_credit_amount/"+$(this).val(), success:function(result){
            jQuery("#models_options").html(result);
            var newOptions = JSON.parse(result);          
            var $select = $("#available_id");
            $select.empty(); // remove old options
            $.each( newOptions, function( value, key ) {
              $select.append($("<option></option>").attr( "value", value ).text(key));
            });           
          }});
        });
          });
        </script>
         <script>
        jQuery(document).ready(function() {
    
        "use strict";
        
        jQuery("#customer_id").change(function(){    
          jQuery.ajax({url:"<?php echo base_url(); ?>credit/get_utility/"+$(this).val(), success:function(result){
            jQuery("#models_options").html(result);
            var newOptions = JSON.parse(result);          
            var $select = $("#utilized_id");
            $select.empty(); // remove old options
            $.each( newOptions, function( value, key ) {
              $select.append($("<option></option>").attr( "value", value ).text(key));
            });           
          }});
        });
          });
        </script>
        <script>
        jQuery(document).ready(function() {
    
        "use strict";
        
        jQuery("#sub_county").change(function(){    
          jQuery.ajax({url:"<?php echo base_url(); ?>facility/get_sub_county/"+$(this).val(), success:function(result){
            jQuery("#models_options").html(result);
            var newOptions = JSON.parse(result);          
            var $select = $("#Sub-County");
            $select.empty(); // remove old options
            $.each( newOptions, function( value, key ) {
              $select.append($("<option></option>").attr( "value", value ).text(key));
            });           
          }});
        });
          });
        </script>

        <script>
$(document).ready(function(){
 $('#country').change(function(){
  var country_id = $('#country').val();
  if(country_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>facility/get_sub_county",
    method:"POST",
    data:{country_id:country_id},
    success:function(data)
    {
     $('#state').html(data);
     $('#city').html('<option value="">Select City</option>');
    }
   });
  }
  else
  {
   $('#state').html('<option value="">Select State</option>');
   $('#city').html('<option value="">Select City</option>');
  }
 });

 $('#state').change(function(){
  var state_id = $('#state').val();
  if(state_id != '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>dynamic_dependent/fetch_city",
    method:"POST",
    data:{state_id:state_id},
    success:function(data)
    {
     $('#city').html(data);
    }
   });
  }
  else
  {
   $('#city').html('<option value="">Select City</option>');
  }
 });
 
});
</script>
<script>
        jQuery(document).ready(function() {
    
        "use strict";
     
        jQuery("#zones").change(function(){  
            
          jQuery.ajax({url:"<?php echo base_url(); ?>facility/wards/"+$(this).val(), success:function(result){
            jQuery("#models_options").html(result);
            var newOptions = JSON.parse(result);          
            var $select = $("#ward");
            $select.empty(); // remove old options
            $.each( newOptions, function( value, key ) {


    </body>
</html>


       
        
      
       <!--  -->
