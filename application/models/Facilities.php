<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Facilities extends CI_Model {
		
		public function __construct(){

			$this->table="facilities";
			$this->datetime=date("Y-m-d H:i:s");
		    $this->id=$this->session->userdata('admin_id');
		    $this->first_name=$this->session->userdata('first_name');
		   
		}

	 public function saves($user_agent,$ip_address){

  	    
  		$data=array(
  			'name' =>$this->input->post("facility_name"),
  			'county_id'=>$this->input->post("country_id"),
  			'subcounty_id'=>$this->input->post("Sub_County_name"),
  			'code'=>1,
  			'date_created'=>$this->datetime,
  			
  			
				 
	   );
							
							
		 if ($this->db->insert( 'facilities',$data )){
		 	$this->db->insert('table_logs', [
								'date_created' => $this->datetime,
								'descriptions' => $this->first_name ." added Facility - ". $this->input->post("name") ,
								'user_id'=>  $this->id,
								'ip'=>$ip_address,
								'agent'=>$user_agent,
								
							] );
			   return true;

		 }else{
		 	$this->db->insert('table_logs', [
								'date_created' => $this->datetime,
								'descriptions' => $this->first_name ." error occurred while adding facility - ". $this->input->post("name") ,
								'user_id'=>  $this->id,
								'ip'=>$ip_address,
								'agent'=>$user_agent,
								
							] );

			 return false;   

	  	}
	 }

	 public function lists()
	{
		//$sub = $this->session->set_userdata( 'subcounty_id' );
		$rows = array();			
		$this->db->select( 'f.*, u.id as admin,u.sub_county_id' );
		$this->db->from('facilities f' )
		         ->join('sys_users u','f.subcounty_id=u.sub_county_id')
		         ->where(array('f.subcounty_id'=>'u.sub_county_id'))
		         ->order_by('f.id','desc');

		$query = $this->db->get();
		if($query->result()){
			$rows = $query->result(); 		
			$query->free_result();
		}
	return( $rows );	
	}
		 public function view()
	{
		//$id = $this->uri->segment(3);
		$rows = array();			
		$this->db->select( '*' );
		$this->db->from('products' )
		     //->where('facility_id', '$id')
		     ->order_by('id','desc');

		$query = $this->db->get();
		if($query->result()){
			$rows = $query->result(); 		
			$query->free_result();
		}
		
		
		return( $rows );	
	}
	public function sub_counties($county)
		{
			$rows = [];
			$this->db->select('*' )
					->from('sub_counties')
					->where('county_id','$county');
			$query = $this->db->get();
			if($query->result()){
				$rows = $query->result();	
				$query->free_result();	
			}		
			return( $rows );	
				
		}

		

		 function get_sub_county($country_id)
		 {
		  $this->db->where('county_id', $country_id);
		  $this->db->order_by('name', 'ASC');
		  $query = $this->db->get('sub_counties');
		  $output = '<option value="">Select State</option>';
		  foreach($query->result() as $row)
		  {
		   $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
		  }
		  return $output;
		 }
	
		
		
		
		
}