<?php 
if( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Authentications extends CI_Model 
{	
	protected $is_logged_in;	
	
	public function __construct() 
	{		
		parent::__construct();
		$this->datetime=date("Y-m-d H:i:s");
		$this->table="admin";
		$this->is_logged_in = $this->session->userdata( 'is_logged_in' );	
	}	
	
	public function login()
	{
		$row = array();
		// $result = array();			
		$this->db->select( 'u.*, s.title' );
		$this->db->from('sys_users u' )
					->join('sys_role s','s.id=u.role_id');
		if( $this->input->post( "email" ) != NULL )
			$this->db->where( array('u.email'=> $this->input->post( "email" )),'AND' );
		$query = $this->db->get();
		if($query->result()){
				$row = $query->row();	
				$query->free_result();	
			}		
			return( $row );
	}
		
	public function logout()
	{
		$data = array( 'is_logged_in', 'user_id', 'user_role_id', 'user', 'id_no', 'service_no', 'station_id' );		
		$this->session->unset_userdata( $data );
		$this->session->sess_destroy();
		
		if( $this->is_logged_in() == true )	
			return false;
		else
			return true;		
	}	
		
	public function is_logged_in()	
	{
		if( !isset( $this->is_logged_in ) or $this->is_logged_in != true  )		
			return false;
		else 
			return true;		
	}
	
	
	public function change_password()
	{
		#$username = $this->input->post("username");
		$password = $this->input->post("password");
		$password1 = $this->input->post("password1");
		$user_id=$_SESSION['user_id'];
		$array=array('id'=>$user_id,'active'=>0);
		$this->db->where($array);
		$this->db->select( '*' )->from( 'users' );
		$query = $this->db->get();
			
		if( $query->num_rows() == 1 ) :
		 
			$result = $query->row();
			$query->free_result();
			$hash = $result->hash;
			$stored_password = $result->password;
			$password = hash( 'sha256', $hash . $password );
			
			if( $stored_password == $password ) : 
				$password = hash( 'sha256', $hash . $password1);
				$data=array(
				'password' =>$password,
				);

				$this->db->where( 'id', $user_id );
				if( $this->db->update('users', $data )):
					return true;
				else:
					return false;
				endif;				
			else :
				return false;				
			endif;
			
		else :	
			return false;
		endif;	
	}

	public function forget_passwords($password,$first_name,$user_id)
		{
			 $data = array(
						
						'password' => password_hash($password, PASSWORD_BCRYPT )
				);
			  $this->db->where('id',$user_id);
			  if($this->db->update($this->table,  $data)){
			  			$this->db->insert('table_logs', [
						'date_created' => $this->datetime,
						'descriptions' => $first_name ." reset password ",
						'user_id'=>  $user_id
						
					] );
					 return true;
			  }else{
				 return false;
			  }	
		}
	
	public function __destruct() 
	{
	
	}	
}