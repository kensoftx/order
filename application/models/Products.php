<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Products extends CI_Model {
		
		public function __construct(){

			$this->table="products";
			$this->datetime=date("Y-m-d H:i:s");
		    $this->id=$this->session->userdata('admin_id');
		    $this->first_name=$this->session->userdata('first_name');
		   
		}

	 public function saves($user_agent,$ip_address){

  	    
  		$data=array(
  			'name' =>$this->input->post("product_name"),
  			'start_balance'=>$this->input->post("start"),
  			'close_balance'=>$this->input->post("close"),
  			
  			
  			
				 
	   );
							
							
		 if ($this->db->insert( 'products',$data )){
		 	$this->db->insert('table_logs', [
								'date_created' => $this->datetime,
								'descriptions' => $this->first_name ." added Product - ". $this->input->post("name") ,
								'user_id'=>  $this->id,
								'ip'=>$ip_address,
								'agent'=>$user_agent,
								
							] );
			   return true;

		 }else{
		 	$this->db->insert('table_logs', [
								'date_created' => $this->datetime,
								'descriptions' => $this->first_name ." error occurred while adding product - ". $this->input->post("name") ,
								'user_id'=>  $this->id,
								'ip'=>$ip_address,
								'agent'=>$user_agent,
								
							] );

			 return false;   

	  	}
	 }
	  public function lists()
	{
		
		$rows = array();			
		$this->db->select( '*' );
		$this->db->from('products' )
		     ->order_by('id','desc');

		$query = $this->db->get();
		if($query->result()){
			$rows = $query->result(); 		
			$query->free_result();
		}
	return( $rows );	
	}

	 public function facilities()
	{
		
		$rows = array();			
		$this->db->select( '*' );
		$this->db->from('facilities' )
		     ->order_by('id','desc');

		$query = $this->db->get();
		if($query->result()){
			$rows = $query->result(); 		
			$query->free_result();
		}
	return( $rows );	
	}
		
	
		
		
		
		
}