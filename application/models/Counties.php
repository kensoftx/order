<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Counties extends CI_Model {
	protected $table;
	public $account = [];
	#protected $id_no;
	public function __construct(){
		
		$this->table="counties";
		$this->table2="sub_counties";
		$this->datetime=date("Y-m-d H:i:s");
	}

	public function create()
	{ $session= $this->session->userdata('admin_id');
		if( $this->db->insert( $this->table, [
			
			'name' => $this->input->post("company"),
			'descr' => $this->input->post("descr"),
			'status' =>1,
			'date' =>$this->datetime, 		
			
		] ) )
			return true;
		else
			return false;		
	}

	 public function saves($user_agent,$ip_address){

  	   
  		if( $this->db->insert( $this->table, [
  			'name' =>$this->input->post("county_name"),
  			'descr' => $this->input->post("description"),
  			'status' =>1,
  			'date' =>$this->datetime,
  			'date_modified' => $this->datetime,
  		
  		] ) )
			return true;
		else
			return false;

     }

     public function sub_counties()
		{
			$rows = [];
			$this->db->select('sb.*,c.id,c.name as county' )
					->from('sub_counties sb')
					->join('counties c', 'c.id=sb.county_id');
			$query = $this->db->get();
			if($query->result()){
				$rows = $query->result();	
				$query->free_result();	
			}		
			return( $rows );	
				
		}

     public function saves_sub_county($user_agent,$ip_address){

  	   
  		if( $this->db->insert( $this->table2, [
  			'county_id' =>$this->input->post("county"),
  			'name' =>$this->input->post("sub-county_name"),
  			'status' =>1,
  			'date' =>$this->datetime,
  			'date_modified' => $this->datetime,
  		
  		] ) )
			return true;
		else
			return false;

     }
	public function backend($state){
		$rows = array();		
		$this->db->select('company_initial');
		//$this->db->where('comp_id', $state);
		$this->db->from('admin');
				//->order_by('name','asc');	
		$query = $this->db->get();
		$rows = $query->row()->company_initial;
		$query->free_result();	
		return( $rows);
	}

	public function company($state){
		$rows = array();		
		$this->db->select('*');
		$this->db->where('comp_id', $state);
		$this->db->from('companies')
				->order_by('name','asc');	
		$query = $this->db->get();
		$rows = $query->row()->name;
		$query->free_result();	
		return( $rows);
	}

 public function counties(){
		$rows = array();		
		$this->db->select('*');
		$this->db->from('counties')
				->order_by('name','asc');	
		$query = $this->db->get();
		$rows = $query->result();
		$query->free_result();	
		return( $rows );
	}
	
	function stations($state){
    $this->db->select('*');
    $this->db->where('co_id', $state);
    $query = $this->db->get('stations');
    //log_message('info', "Value of state was: $state");
    if ($query->num_rows() > 0)
    {
      return $query->result_array();
    }else{
      return false;
    }
}  
	
	 public function detail_companies($id){
		$rows = array();		
		$this->db->select('*');
		$this->db->from('companies')
		->where('comp_id',$id);
		$query = $this->db->get();
		$rows = $query->row();
		$query->free_result();
		//print_r($rows);exit;
		return( $rows );

	}
	
	

	public function edit_companies($id)
		{
			$id= $this->session->userdata('admin_id');
			 $data = array(
						
						'name' => $this->input->post("company_name"),
						'descr' => $this->input->post("description"),
						'date_modified' => $this->datetime,
				
				);
			  $this->db->where('comp_id',$id);
			  if($this->db->update($this->table,  $data)){
			  		$this->db->insert('table_logs', [
						'date_created' => $this->datetime,
						'descriptions' => $this->first_name ." updated company ".$this->input->post("company_name"). " details",
						'user_id'=>  $id,
						
					] );
					 return true;
			  }else{
				 return false;
			  }	
		}
	
	public function list_companies(){
		$rows = array();		
		$this->db->select("*");
		$this->db->from( $this->table .' u');
				//->join('sys_roles r','r.id=u.role_id');
		$this->db->where('u.active',1);		
		$query = $this->db->get();
		$rows = $query->result();
		$query->free_result();		
		return( $rows );
	}
	

	public function deletee_companies($id)
	{    
 //echo $id;exit;
		  $this->db->where('comp_id',$id);
		  if($this->db->delete("companies")){
				 return true;
		  }else{
			 return false;
	
}
}

public function orders()
		{
			$co = $this->input->post('company');
			$std = $this->input->post('station');
			$session= $this->session->userdata('admin_id');
			$rows =array();	
			$this->db->select( 'DISTINCT(o.counter),o.doc,o.order_status,o.fin_status,o.confirm,o.transit,o.offload,s.name, s.location,f.comment,a.*' )
				 ->from('order o')
				 ->join('files  f', 'f.lpo= o.counter')
				 ->join('admin  a', 'a.id= o.cust_id')
				 ->join('stations  s', 's.sta_id= o.company')
				 ->where(array('o.counter !='=>'','o.doc !='=>'','o.company'=>$std,'o.co_id'=>$co));
			$query = $this->db->get();
			if($query->result()){
				$rows = $query->result();			
				$query->free_result();
			}
			return $rows;
		}
}