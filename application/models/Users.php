<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Users extends CI_Model {
		
		public function __construct(){
			
			$this->table="sys_users";
			$this->datetime=date("Y-m-d H:i:s");
		   
		   $this->id=$this->session->userdata('admin_id');
		   $this->first_name=$this->session->userdata('first_name');
		}
	
		public function lists()
		{


		$session= $this->session->userdata('role_id');
		$co_id= $this->session->userdata('co_id');
		$std_id= $this->session->userdata('sta_id');

		$rows = [];
		  
		
		   $this->db->select('u.*, r.title as role');
			$this->db->from( $this->table .' u')
					->join('sys_role r','r.id=u.role_id');
		    if($_SESSION['role_id']==2 || $_SESSION['role_id']==1){       
		    	
		    	$this->db->order_by('created_on','desc');
			    $query=$this->db->get();
			    if($query->result()){

					$rows = $query->result();	
					$query->free_result();	
				}	
		    } elseif ($_SESSION['role_id']==8){

		    		$this->db->where(array( 'u.co_id'=>$co_id, 'u.sta_id'=>$std_id,'r.id>'=>8));
		    		$this->db->order_by('created_on','desc');
				    $query=$this->db->get();
				    if($query->result()){

						$rows = $query->result();	
						$query->free_result();	
					}	
		    	

		    }elseif($_SESSION['role_id']==11 || $_SESSION['role_id']==9){

		    	$this->db->where(array( 'u.co_id'=>$co_id, 'u.sta_id'=>$std_id,'r.id'=>10));
		    	$this->db->order_by('created_on','desc');
				$query=$this->db->get();
				    if($query->result()){

						$rows = $query->result();	
						$query->free_result();	
					}	
		    	
		    }else{

		    	$this->db->where(array('c.co_id'=>2000,'sta_id'=> 100000));
		    	$this->db->order_by('created_on','desc');
				$query=$this->db->get();
				    if($query->result()){

						$rows = $query->result();	
						$query->free_result();	
					}	

		    }

		 	
			return( $rows );
		






        			
		}
	
		public function roles()
		{
			$rows = [];
			$this->db->select('*' )
					->from('sys_role');
			
			
			$query = $this->db->get();
			if($query->result()){
				$rows = $query->result();	
				$query->free_result();	
			}		
			return( $rows );	
				
		}
	

		
		public function detail_user($id){
			$row = array();		
			$this->db->select('*');
			$this->db->from( $this->table);	
			$this->db->where('id',$id);
			$query = $this->db->get();
			if($query->result()){
				$row = $query->row();	
				$query->free_result();	
			}		
			return( $row );
		}
		
		public function deletee($id)
		{
			 $data = array(
						'date_modified' => $this->datetime,
						'status' => 0,	
				);
			  $this->db->where('id',$id);
			  if($this->db->update($this->table,  $data)){
			  			$this->db->insert('logs', [
						'date_created' => $this->datetime,
						'descriptions' => $this->first_name ." deletes system user id - ".$id ,
						'user_id'=>  $this->id
						
					] );
					 return true;
			  }else{
				 return false;
			  }	
		}
		public function edit_users($id)
		{
			
			 $data = array(
						'last_edited' => $this->datetime,
						'first_name' => $this->input->post("fname"),
						'last_name' => $this->input->post("lname"),
						'mobile1' => $this->input->post("mobile"),
						'email' => $this->input->post("email"),
						'role_id' => $this->input->post("role_id"),	

				);
			  $this->db->where('id',$id);
			  if($this->db->update($this->table,  $data)){
			  		$this->db->insert('table_logs', [
						'date_created' => $this->datetime,
						'descriptions' => $this->first_name ." updated system user details",
						'user_id'=>  $this->id,
						'ip'=>$ip_address,
						'agent'=>$user_agent,
						
					] );
					 return true;
			  }else{
				 return false;
			  }	
		}
		public function create($user_agent,$ip_address)
		{
			
			
				
				if( $this->db->insert( $this->table, [
					'created_on' => $this->datetime,
					'last_edited' => $this->datetime,
					'first_name' => $this->input->post("fname"),
					'last_name' => $this->input->post("lname"),
					'mobile1' => $this->input->post("mobile"),
					'email' => $this->input->post("email"),
					'role_id' => $this->input->post("role_id"),
					'county_id' => $this->input->post("county_id"),
					'sub_county_id' => $this->input->post("subcounty_id"),	
					'password' => password_hash( $this->input->post( "password" ), PASSWORD_BCRYPT ),
					
				] ) ){
				
			
				  $this->db->insert('table_logs', [
						'date_created' => $this->datetime,
						'descriptions' => $this->first_name ." Add  system user  details",
						'user_id'=>  $this->id,
						'ip'=>$ip_address,
						'agent'=>$user_agent,
						
					] );
				  return true;
				
				}else{
					return false;
				}

				
				
			
		}
		private function sendemail($data)
		{
			$message = array();
			$message['email'] = $data['email'];
			$message['subject'] = "Account Registration ";
			$message['message'] = "You have been successfully registered on the Mpesa systems solutions. Login in using " . $data['email'].". Your default password is ".$data['password'].". Change it on the first login. Thank you.";
			$message['name'] = $data['name'];
			$result = $this->emails->sendmail($message);
			return $result;
			


		}
		public function changepassword(){
		$this->db->where('id', $_SESSION['id']);
		$data=array('password' => password_hash( $this->input->post( "newpassword" ), PASSWORD_BCRYPT ),
			'activate' =>1,
			);
			if($this->db->update($this->table, $data)):
				return true;
			else:
				return false;
			endif;

		}
		public function get_oldpass(){
			$row=array();

			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where(['id'=>$_SESSION['id']]);
			$query = $this->db->get();
			$row = $query->row();
			if ($query->num_rows()>0){
				$password= $row->password;
				if(( password_verify( $this->input->post( "password" ), $password ) )){

					  $this->db->insert('logs', [
						'date_created' => $this->datetime,
						'descriptions' => $this->first_name ." changed password",
						'user_id'=>  $this->id
						
					] );
					return TRUE;
				}else{
					return FALSE;
				}
			}else{
				$query->free_result();
				return FALSE;
			}


		}

	public function deletee_users($id)
	{

		$rows = array();		
			$this->db->select('*');
			$this->db->from('sys_users')->where('id',$id);	
			$query = $this->db->get();
			$row = $query->row();
			$query->free_result();			
		 $data = array(
				'last_edited' => $this->datetime,
				'account_status' => 0,	
			);
		  $this->db->where('id',$id);
		  if($this->db->update("sys_users",  $data)){

		  	$this->db->insert('table_logs', [
						'date_created' => $this->datetime,
						'descriptions' => $this->first_name ." deactivated ". $name ,
						'user_id'=>  $this->id
						
					] );
				 return true;
		  }else{
			 return false;
		  }
		  
	}
	public function activate_users($id)
	{

		$rows = array();		
			$this->db->select('*');
			$this->db->from('sys_users')->where('id',$id);	
			$query = $this->db->get();
			$row = $query->row();
			$query->free_result();			
		 $data = array(
				'last_edited' => $this->datetime,
				'account_status' => 1,	
			);
		  $this->db->where('id',$id);
		  if($this->db->update("sys_users",  $data)){

		  	$this->db->insert('table_logs', [
						'date_created' => $this->datetime,
						'descriptions' => $this->first_name ." activated ". $name ,
						'user_id'=>  $this->id
						
					] );
				 return true;
		  }else{
			 return false;
		  }
		  
	}
		
		
		
		
		
}